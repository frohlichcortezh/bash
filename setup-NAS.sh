#!/usr/bin/env bash

# update
sudo apt-get update -y

# upgrade
sudo apt-get upgrade -y

# install useful tools

# install useful terminal
# ToDo give choice
. setup-terminal.sh

sudo apt install nfs-common -you
sudo apt-get remove samba --purge -y
sudo apt-get purge samba-common -y
sudo apt-get autoremove -y

sudo apt-get install samba samba-common-bin -y

echo edit smb.conf file with 'sudo nano /etc/samba/smb.conf'
echo just under ### Authentication ### add security = user
echo ex. 
echo '####### Authentication #######'
echo security = user

echo when you done, run sudo /etc/init.d/smbd restart

echo edit smb.conf file with 'sudo nano /etc/samba/smb.conf'

echo '# By default, the home directories are exported read-only. Change the'
echo '# next parameter to no if you want to be able to write to them. '
echo '  read only = no '

sudo smbpasswd -a pi

# add new users
# sudo useradd john -m -G users
# then give the user a password
# sudo passwd john
# then give them a sambapasswd
# sudo smbpasswd -a john

# check for your external drive
# sudo blkid

# something like 
# /dev/sda2: LABEL="PiNAS-DATA" UUID="B82456932456550A" TYPE="ntfs" PARTLABEL="Basic data partition" PARTUUID="bc811970-d01f-4acd-92c9-ffb387ee2d87"

# edit fstab
# sudo nano /etc/fstab

# sudo mount -t ntfs -o rw,auto,user,fmask=0022,dmask=0000 /dev/whatever /mnt/whatever
# UUID=FEDC5DB5DC5D6943   /media/software   ntfs    defaults,auto,umask=002      0      0
# UUID=B82456932456550A   /mnt/nas     ntfs-3g   defaults,nls=utf8,umask=000,dmask=027,fmask=137,uid=1000,gid=1000,windows_names 0 0 