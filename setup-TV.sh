#!/usr/bin/env bash

#import libutils functions

source libutils.sh

sudo apt-get install xscreensaver fuse -y

# create auto-start file
sudo touch /etc/xdg/autostart/screensaver.desktop
sudo cat > /etc/xdg/autostart/screensaver.desktop << EOF
[Desktop Entry]
Name=Screensaver
Type=Applicaton
Exec=xscreensaver -nosplash
EOF

mkdir $HOME/dev/scripts/install
cd $HOME/dev/scripts/install

wget -qO- https://raw.githubusercontent.com/Botspot/pi-apps/master/install | bash

# install snapd
sudo apt-get install snapd -y
sudo reboot
sudo snap install core

# install other tools
sudo apt-get install keepassxc alsa-utils avahi-daemon kodi ntfs-3g ufw -y
sudo apt install kodi-peripheral-joystick kodi-pvr-dvblink -y

sudo systemctl disable avahi-daemon

sudo su
yourIpAddress=$(hostname  -I | cut -f1 -d' ')
echo $yourIpAddress LaCasa-NAS >> /etc/avahi/hosts
exit

sudo systemctl start avahi-daemon

# mopidy

wget -q -O - https://apt.mopidy.com/mopidy.gpg | sudo apt-key add -
sudo wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/buster.list

sudo apt update -y
sudo apt install mopidy python3-pip -y

sudo apt install mopidy-spotify mopidy-scrobbler mopidy-soundcloud mopidy-youtube mopidy-tunein mopidy-mpd mopidy-local-sqlite mopidy-local mopidy-internetarchive -y

sudo systemctl enable mopidy
sudo systemctl start mopidy

sudo python3 -m pip install Mopidy-Iris
sudo sh -c 'echo "mopidy ALL=NOPASSWD: /usr/local/lib/python3.7/dist-packages/mopidy_iris/system.sh" >> /etc/sudoers'

enableSpotify=""
spotifyUser=""
spotifyPwd=""
spotifyClient_id=""
spotifyClient_secret=""

inputSpotify() {
    f_readYesNo "Would you like to connect mopidy to a spotify account ? (Y/n) "
    if [ $? -eq 0 ]; then enableSpotify=$f_dialog_RETURNED_VALUE; fi
    
    if [ "$yes_no" != "Y" ] then
        echo "not setting up spotify"
        enableSpotify=false
        return
    else
        enableSpotify=true
    fi            

    echo visit https://mopidy.com/ext/spotify/ and authenticate to spotify to get your client_id and client_secret indices

    f_dialog_input "Please inform your spotify username: "
    if [ $? -eq 0 ]; then spotifyUser=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$spotifyUser" = "" ]; do read spotifyUser; done

    f_dialog_pwd "Please inform your spotify password: "
    if [ $? -eq 0 ]; then spotifyPwd=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$spotifyPwd" = "" ]; do read spotifyPwd; done

    f_dialog_input "Please inform your spotify client id: "
    if [ $? -eq 0 ]; then spotifyClient_id=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$spotifyClient_id" = "" ]; do read spotifyClient_id; done        

    f_dialog_input "Please inform your spotify client secret: "
    if [ $? -eq 0 ]; then spotifyClient_secret=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$spotifyClient_secret" = "" ]; do read spotifyClient_secret; done            
}

enableLastFM=""
lastFMUser=""
lastFMPwd=""

inputLastFM() {
    f_readYesNo "Would you like to connect mopidy to a LastFM account ? (Y/n) "
    if [ $? -eq 0 ]; then enableLastFM=$f_dialog_RETURNED_VALUE; fi
    
    if [ "$yes_no" != "Y" ] then
        echo "not setting up Last.FM"
        enableLastFM=false
        return
    else
        enableLastFM=true
    fi            

    f_dialog_input "Please inform your LastFM username: "
    if [ $? -eq 0 ]; then lastFMUser=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$lastFMUser" = "" ]; do read lastFMUser; done

    f_dialog_pwd "Please inform your LastFM password: "
    if [ $? -eq 0 ]; then lastFMPwd=$f_dialog_RETURNED_VALUE; fi
    
    while [ "$lastFMPwd" = "" ]; do read lastFMPwd; done
}

inputSpotify

inputLastFM

cat > ~/.bashrc << EOF
[http]
enabled = true
hostname = 0.0.0.0
port = 6680
zeroconf = Mopidy HTTP server on $hostname
allowed_origins = 
csrf_protection = true
default_app = iris

[spotify]
enabled = $enableSpotify
username = $spotifyUser
password = $spotifyPwd
client_id = $spotifyClient_id
client_secret = $spotifyClient_secret
bitrate = 320
volume_normalization = true
private_session = false
timeout = 10
allow_cache = true
allow_network = true
allow_playlists = true
search_album_count = 20
search_artist_count = 10
search_track_count = 50
toplist_countries = 

[scrobbler]
enabled = $enableLastFM
username = $lastFMUser
password = $lastFMPwd
EOF

# mount ntfs drive
# https://support.plex.tv/articles/200288606-mounting-ntfs-drives-on-linux/


# manual seatings

echo "
if youtube videos are not playing on chromium, check settings > advance settings > system (chrome://settings/system)
try disabling ''Use hardware acceleration when available'' then relaunch
"

echo "
launch 'sudo raspi-config'

go to 4. performance options > P2 GPU Memory >  256 MB

GO TO Interfacing Options > Camera > Yes
"