#!/usr/bin/env bash

sudo apt-get update -y && sudo apt-get install upgrade -y

sudo apt-get install git curl bash fish guake -y

# install powerline-shell
# https://github.com/b-ryan/powerline-shell
pip install powerline-shell

# must add pip path to path
# nornmaly /home/pi/.local/bin

# CHECK FOR vs code server 
#echo "export PATH=/home/pi/.vscode-server/bin/83bd43bc519d15e50c4272c6cf5c1479df196a4d/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/snap/bin:/home/pi/.local/bin" >> ~/.bashrc
#echo "export PATH=/home/pi/.vscode-server/bin/83bd43bc519d15e50c4272c6cf5c1479df196a4d/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/snap/bin:/home/pi/.local/bin" >> ~/.bash_profile 
#echo "PATH=/home/pi/.vscode-server/bin/83bd43bc519d15e50c4272c6cf5c1479df196a4d/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/snap/bin:/home/pi/.local/bin" >> /etc/environment

cat > ~/.bashrc << EOF
function _update_ps1() {
    PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi
EOF

cat > ~/.config/fish/config.fish << EOF
function fish_prompt
    powerline-shell --shell bare $status
end
EOF