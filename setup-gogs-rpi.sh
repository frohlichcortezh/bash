#!/usr/bin/env bash

# References
# - https://github.com/gogs/gogs/tree/main/docker

# ToDo 
# - Verificar que `docker` esta instalado
# - Perguntar se gostaria de instalar docker ou continuar com outro processo
# - Por enquanto apenas com docker, outros processos a implementar

docker pull gogs/gogs-rpi

# ToDo
# - Verificar se variavel $DEV_DIR contem valor, caso contrario definir como $HOME/dev
mkdir -p $DEV_DIR/docker/volumes/gogs

docker run --name=gogs -p 10022:22 -p 10080:3000 -v $DEV_DIR/docker/volumes/gogs:/data gogs/gogs-rpi

docker run --name=gogs -p 2322:22 -p 3000:3000 -v $HOME/dev/docker/volumes/gogs:/data gogs/gogs-rpi

#Setup LDAP
# cn=%s,ou=users,dc=lacasa,dc=com
# uid=%s,ou=users,dc=lacasa,dc=com