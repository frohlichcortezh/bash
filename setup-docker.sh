#!/usr/bin/env bash

# referencias:
# https://pratiquepc.fr/comment-installer-docker-et-portainer-sur-un-raspberry-pi/
# https://dxpetti.com/blog/2019/docker-daemon-failing-to-start-on-raspberry-pi-1-zeros/
# https://linuxhint.com/install_docker_raspberry_pi-2/
# https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script

# cleaning up
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get purge docker-ce
sudo apt update && sudo apt upgrade -VV

sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release

mkdir $HOME/dev/scripts/install
cd $HOME/dev/scripts/install

wget http://ftp.us.debian.org/debian/pool/main/libs/libseccomp/libseccomp2_2.4.4-1~bpo10+1_armhf.deb
sudo dpkg -i libseccomp2_2.4.4-1~bpo10+1_armhf.deb

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# add curent use to docker group
sudo usermod -aG docker $USER

# setup portainer
sudo docker pull portainer/portainer:linux-arm
sudo docker run --restart always -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer:linux-arm

cd $HOME/dev/scripts/install

yourIpAddress=$(hostname  -I | cut -f1 -d' ')

echo Visit http://$yourIpAddress:9090 and finish configuring portainer. Configure admin user and choose local

