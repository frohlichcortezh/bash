#!/usr/bin/env bash

# install vs code
sudo apt-get install code -y

# Instalar NVM, NodeJS, npm e yarn para desenvolvimento JS (Angular, Vue, etc)
# Referencias:
# - https://yarnpkg.com/getting-started/install

sudo apt purge -y nodejs.*
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

nvm -v
nvm install --lts
node -v
npm -v
npm install -g yarn