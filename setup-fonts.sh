#!/usr/bin/env bash

sudo apt-get install fontconfig -y

mkdir $HOME/fonts
mkdir -p ~/.local/share/fonts

cd $HOME/fonts
curl -fsSL https://github.com/microsoft/cascadia-code/releases/download/v2108.26/CascadiaCode-2108.26.zip -o CascadiaCode.zip
unzip CascadiaCode.zip -d CascadiaCode
cd CascadiaCode/ttf
mv *.ttf ~/.local/share/fonts/

fc-cache -f -v

cd $HOME/fonts 
rm CascadiaCode -rf